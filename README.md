# jenkins-docker

https://hub.docker.com/r/bitnami/jenkins/

Environment variables

The Jenkins instance can be customized by specifying environment variables on the first run. The following environment values are provided to customize Jenkins:

    JENKINS_USERNAME: Jenkins admin username. Default: user
    JENKINS_PASSWORD: Jenkins admin password. Default: bitnami
    JENKINS_HOME: Jenkins home directory. Default: /opt/bitnami/jenkins/jenkins_home
    DISABLE_JENKINS_INITIALIZATION: Allows to disable the initial Bitnami configuration for Jenkins. Default: no
    JAVA_OPTS: Customize JVM parameters. No defaults.
